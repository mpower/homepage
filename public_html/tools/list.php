<?php
$pageTitle = 'Shopping List';
$pageDescription = 'House Shopping List';
include '../inc/loader.php';
include '../inc/header.php';
include '../inc/nav.php';

$listId = "216779059";
$query = array(
    "subtasks" => "true",
    "reverse" => "true",
);

$curl = curl_init();

curl_setopt_array($curl, [
    CURLOPT_HTTPHEADER => [
        "Authorization: pk_32308626_FT13TMGI7MH5TZUZ18KFRD3BF0XY1YVZ",
        "Content-Type: application/json"
    ],
    CURLOPT_URL => "https://api.clickup.com/api/v2/list/" . $listId . "/task?" . http_build_query($query),
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_CUSTOMREQUEST => "GET",
]);

$response = curl_exec($curl);
$error = curl_error($curl);

curl_close($curl);

if ($error) {
    echo "cURL Error #:" . $error;
}

?>

    <section id="words" class="container page-start">
        <div class="row">
            <div class="col text-center">
                <h1 class="page-header">Shopping List</h1>
            </div>
        </div>
        <div class="row">
            <div class="col mx-auto text-center">
                <?php
                $json = json_decode($response, true);
                foreach ($json["tasks"] as $task) {
                    if //parent task
                    ($task["parent"] != null) {
                        echo "<h2>" . $task["name"] . "</h2>";
                        // foreach checklist item
                        foreach ($task["checklists"] as $checklist) {
                            ?>
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Item</th>
                                    <th scope="col">Edit</th>
                                    <th scope="col">Delete</th>
                                </tr>
                                </thead>
                                <?php
                                $i = 1;
                                foreach ($checklist["items"] as $checkItem) {
                                    ?>
                                    <tr>
                                        <th scope="row"><?= $i++; ?></th>
                                        <td><?= ($checkItem["resolved"] == "true") ? "<strike>" . $checkItem["name"] . "</strike>" : $checkItem["name"]; ?></td>
                                        <td>Edit</td>
                                        <td>Delete</td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                <tr>
                                    <th scope="row">NEW</th>
                                    <td><input type="text"></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                            <?php
                        }
                    }
                }
                ?>
            </div>
        </div>
    </section>

<?php
include '../inc/footer.php';