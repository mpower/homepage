<?php
$pageTitle = 'Find Words';
$pageDescription = 'A simple tool for words';
include '../inc/loader.php';
include '../inc/header.php';
include '../inc/nav.php';
?>

    <section id="words" class="container page-start">
        <div class="row">
            <div class="col text-center">
                <h1 class="page-header">nth Word Extraction</h1>
                <p>A simple function to extract the nth word from text.</p>
            </div>
        </div>
        <div class="row">
            <div class="col mx-auto text-center">
                <?php
                if (isset($_POST["text"])) {

                    $text = $_POST["text"];
                    $nth = $_POST["nth"];

                    $text = preg_replace('/\p{P}/', '', $text);

                    $textArray = explode(" ", $text);
                    $count = 0;
                    ?>
                    <div class="row">
                        <div class="col mx-auto text-center border mb-2">
                            <p>
                                <?php
                                foreach ($textArray as $word) {
                                    $count++;
                                    if ($count % $nth == 0) {
                                        echo $word, " ";
                                    }
                                }
                                ?>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col mx-auto text-center">
                            <form name="words" id="form" action="" method="POST">
                                <div class="row">
                                    <div class="col-auto mx-auto mb-2 alert alert-secondary">
                                        <div class="row mb-2">
                                            <div class="col-3">
                                                <label class="col-form-label" for="text">Text</label>
                                            </div>
                                            <div class="col">
                                                <textarea class="form-control" name="text"
                                                          id="text" cols="60" rows="5" required><?= $text; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-3">
                                                <label class="col-form-label" for="nth">nth</label>
                                            </div>
                                            <div class="col">
                                                <input type="number" class="form-control" value="<?= $nth; ?>"
                                                       name="nth"
                                                       id="nth"
                                                       required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col text-center">
                                        <button class="mt-3 btn btn-lg btn-success btn-block">
                                            <i class="fa fa-paper-plane"></i> Get
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <?php

                } else {
                    ?>
                    <div class="row">
                        <div class="col mx-auto text-center">
                            <form name="words" id="form" action="" method="POST">
                                <div class="row">
                                    <div class="col-auto mx-auto text-center alert alert-secondary form-group">
                                        <div class="row mb-2">
                                            <div class="col-3">
                                                <label class="col-form-label" for="text">Text</label>
                                            </div>
                                            <div class="col">
                                                <textarea class="form-control" name="text" id="text" cols="60" rows="5"
                                                          required></textarea>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-3">
                                                <label class="col-form-label" for="nth">nth</label>
                                            </div>
                                            <div class="col">
                                                <input type="number" class="form-control" name="nth" id="nth" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col text-center">
                                        <button class="mt-3 btn btn-lg btn-success btn-block">
                                            <i class="fa fa-paper-plane"></i> Get
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </section>
<?php
include '../inc/footer.php';