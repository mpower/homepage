<?php
$pageTitle = 'Portfolio';
$pageDescription = 'Project Portfolio of';
include 'inc/loader.php';
include 'inc/header.php';
include 'inc/nav.php';
?>
    <section class="container page-start">
        <div class="row">
            <div class="col">
                <h1 class="page-header text-center">Current Projects</h1>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col mx-auto text-center">
                <h2>Open Source</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-3 mx-auto text-center">
                <div class="card">
                    <img class="card-img-top img-fluid" src="img/portfolio/acuparse.png" alt="Acuparse">
                    <div class="card-body">
                        <h5 class="card-title">Acuparse</h5>
                        <p class="card-text">AcuRite Access/smartHUB and IP Camera Processing, Display, and Upload</p>
                        <a href="https://www.acuparse.com" class="btn btn-primary">Project Site</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
include 'inc/footer.php';
