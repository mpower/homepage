<?php
if(!defined('recaptcha')) {
    header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found");
    exit();
}

$captchaSecret = '';
$captchaSiteKey = '';
