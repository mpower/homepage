<!-- Footer -->

<footer class="container-fluid bg-secondary">
    <div class="row" id="footer_nav_links">
        <div class="col-auto mx-auto">
            <div class="btn-group flex-wrap" role="group" aria-label="Footer Links">
                <button class="btn btn-large btn-secondary"
                        onclick="location.href='/'">
                    <i class="fa-solid fa-home" aria-hidden="true"></i> Home
                </button>
                <?php if ($_SERVER['PHP_SELF'] === '/blog/index.php') { ?>
                    <button class="btn btn-large btn-secondary"
                            onclick="location.href='/blog'">
                        <i class="fa-solid fa-newspaper" aria-hidden="true"></i> Blog
                    </button>
                <?php } ?>
                <button class="btn btn-large btn-secondary"
                        onclick="location.href='/portfolio'">
                    <i class="fa-solid fa-briefcase" aria-hidden="true"></i> Portfolio
                </button>
                <button class="btn btn-large btn-secondary"
                        onclick="location.href='/resume'">
                    <i class="fa-solid fa-file" aria-hidden="true"></i> Resume
                </button>
                <button class="btn btn-large btn-secondary"
                        onclick="location.href='/tree'">
                    <i class="fa-solid fa-tree" aria-hidden="true"></i> Tree
                </button>
                <button class="btn btn-large btn-secondary"
                        onclick="location.href='/contact'">
                    <i class="fa-solid fa-envelope" aria-hidden="true"></i> Contact
                </button>
            </div>
        </div>
    </div>

    <div class="row" id="footer_profile_links">
        <div class="col-auto mx-auto">
            <div class="btn-group flex-wrap" role="group" aria-label="Footer Profile Links">
                <button class="btn btn-secondary"
                        onclick="window.open('https://www.facebook.com/maxtpower')"
                        title="Facebook">
                    <i class="fab fa-facebook"></i>
                </button>
                <button class="btn btn-secondary"
                        onclick="window.open('https://www.instagram.com/maxwelltpower')"
                        title="Instagram">
                    <i class="fab fa-instagram"></i>
                </button>
                <button class="btn btn-secondary"
                        onclick="window.open('https://www.twitter.com/maxtpower')"
                        title="Twitter">
                    <i class="fab fa-twitter"></i>
                </button>
                <button class="btn btn-secondary"
                        onclick="window.open('https://www.pinterest.com/maxtpower')"
                        title="Pinterest">
                    <i class="fab fa-pinterest"></i>
                </button>
                <button class="btn btn-secondary"
                        onclick="window.open('https://www.linkedin.com/in/maxwellpower')"
                        title="Linkedin">
                    <i class="fab fa-linkedin"></i>
                </button>
                <button class="btn btn-secondary"
                        onclick="window.open('https://github.com/maxwellpower')"
                        title="GitHub">
                    <i class="fab fa-github"></i>
                </button>
                <button class="btn btn-secondary"
                        onclick="window.open('https://gitlab.com/mpower')"
                        title="GitLab">
                    <i class="fab fa-gitlab"></i>
                </button>
                <button class="btn btn-secondary"
                        onclick="window.open('https://wa.me/17204621214')"
                        title="Whatsapp">
                    <i class="fab fa-whatsapp"></i>
                </button>
                <button class="btn btn-secondary"
                        onclick="window.open('https://t.me/maxwelltpower')"
                        title="Telegram">
                    <i class="fab fa-telegram"></i>
                </button>
                <button class="btn btn-secondary"
                        onclick="window.open('https://keybase.io/maxpower')"
                        title="Keybase">
                    <i class="fab fa-keybase"></i>
                </button>
                <button class="btn btn-secondary"
                        onclick="window.open('https://www.dropbox.com/request/cuDlgUFhGEVj0l73YJ6U')"
                        title="Dropbox Upload">
                    <i class="fab fa-dropbox"></i>
                </button>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-auto mx-auto text-light">
            <p>Copyright &copy; <?= date("Y"); ?> Maxwell T. Power</p>
        </div>
    </div>

</footer>

<!-- JS -->
<script src="../js/jquery/jquery.min.js"></script>
<script defer src="../js/bootstrap/bootstrap.bundle.min.js"></script>
<script async src="../js/fontawesome/all.min.js"></script>
<script defer src="../js/instantpage/instantpage.js"></script>

</body>
</html>

<!--
“All our dreams can come true, if we have the courage to pursue them.” – Walt Disney
-->
