<?php
$pageTitle = 'Contact Me';
$pageDescription = 'Contact';
include 'inc/loader.php';

// Process Contact Form
if (isset($_GET['do'])) {
    if (file_exists('config/recaptcha.php')) {
        // Check that Google captcha is correct
        $captcha = $_POST['g-recaptcha-response'];
        define('recaptcha', true);
        if (file_exists('config/recaptcha.php')) {
            include('config/recaptcha.php');
        }

        if (!isset($captchaSecret)) {
            exit(1);
        }
        try {
            $captchaResponse = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$captchaSecret&response=$captcha"), true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            exit(1);
        }
    } else {
        $captchaResponse['success'] = true;
    }

    // Captcha Success, process the contact form
    if ($captchaResponse['success'] === true) {

        $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
        $email = strtolower(filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL));
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $_SESSION['messages'] = '<div class="alert alert-danger text-center"><a href="#" class="close" data-dismiss="alert">&times;</a>Error: ' . $email . ' is not a valid email</div>';
            header("Location: /contact");
            die;
        }
        if ($_POST['company']) {
            $_SESSION['messages'] = '<div class="alert alert-warning text-center"></a>Success: Message Sent</div>';
            header("Location: /");
            die;
        }
        $post_subject = filter_input(INPUT_POST, 'subject', FILTER_SANITIZE_STRING);
        $subject = 'Contact Form Submission - ' . $post_subject;
        $message = htmlentities(filter_input(INPUT_POST, 'message', FILTER_SANITIZE_STRING));

        $message = '<p><strong>You have received a new message from:</strong> <a href="mailto:' . $email . '?subject=' . $post_subject . '">' . $name . ' &lt;' . $email . '&gt;</a></p><p>' . $message . '</p>';

        // Mail it
        $to = 'max@maxtpower.com';
        $headers = 'MIME-Version: 1.0' . PHP_EOL;
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . PHP_EOL;
        $headers .= 'From: ' . $name . ' <' . $email . '>' . PHP_EOL;
        $headers .= 'X-Mailer: maxtpower.com' . PHP_EOL;
        $headers .= 'Reply-to: ' . $name . ' <' . $email . '>' . PHP_EOL;

        $messageHeader = "<html><head><title>$subject</title></head><body>";
        $messageFooter = '</body></html>';
        $message = $messageHeader . $message . $messageFooter;

        // Make it so
        mail($to, $subject, $message, $headers);

        // Display message
        $_SESSION['messages'] = '<div class="alert alert-success text-center"></a>Success: Message Sent</div>';
        header("Location: /");
    } else {
        $_SESSION['messages'] = '<div class="alert alert-danger text-center">Error: Invalid captcha on contact form</div>';
        header("Location: /contact");
    }
    die;
} else {
    include 'inc/header.php';
    include 'inc/nav.php';
    ?>
    <section class="container page-start">
        <div class="row">
            <div class="col">
                <h1 class="page-header text-center">Contact Maxwell Power</h1>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-7 mx-auto contact text-center alert alert-secondary">
                <form name="message" id="recaptcha-form" action="/contact?do" method="POST">
                    <div class="row mb-2">
                        <div class="col-3">
                            <label class="col-form-label" for="name">Your Name</label>
                        </div>
                        <div class="col">
                            <input type="text"
                                   class="form-control" name="name" id="name" placeholder="Name">
                        </div>
                    </div>
                    <div class="row mb-2 company">
                        <div class="col-3">
                            <label class="col-form-label" for="company">Company</label>
                        </div>
                        <div class="col">
                            <input type="text" class="form-control" name="company" id="company" placeholder="Company">
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-3">
                            <label class="col-form-label" for="email">Email Address</label>
                        </div>
                        <div class="col">
                            <input type="email" class="form-control" name="email" id="email" placeholder="Email"
                                   required>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-3">
                            <label class="col-form-label" for="subject">Subject</label>
                        </div>
                        <div class="col">
                            <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject"
                                   required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            <label class="col-form-label" for="message">Message</label>
                        </div>
                        <div class="col">
                        <textarea rows="10" cols="100" class="form-control" name="message" id="message"
                                  placeholder="Message"
                                  required></textarea>
                        </div>
                    </div>
                    <?php
                    if (file_exists('config/recaptcha.php')) {
                        define('recaptcha', true);
                        include('config/recaptcha.php');
                        /** @var string $captchaSiteKey */
                        ?>
                        <button type="submit" class="mt-3 btn btn-outline-success g-recaptcha"
                                data-sitekey="<?= $captchaSiteKey; ?>" data-callback="onSubmit"><i
                                    class="fa-solid fa-paper-plane"></i> Send Message
                        </button>
                        <?php
                    } else {
                        ?>
                        <button type="submit" class="mt-3 btn btn-outline-success"><i
                                    class="fa-solid fa-paper-plane"></i> Send Message
                        </button>
                        <?php
                    }
                    ?>
                </form>
            </div>
        </div>
    </section>
    <?php
    if (file_exists('config/recaptcha.php')) {
        ?>
        <script>
            function onSubmit(token) {
                document.getElementById("recaptcha-form").submit();
            }
        </script>
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
        <?php
    }

    // Get footer
    include('inc/footer.php');
}
