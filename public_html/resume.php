<?php
$pageTitle = 'Resume';
$pageDescription = 'Resume of';
include 'inc/loader.php';
include 'inc/header.php';
include 'inc/nav.php';

$jobDetails = [
    0 => ['company' => 'Mattermost', 'url' => '//mattermost.com', 'title' => 'Senior Customer Engineer', 'dates' => 'January 2023 - Present', 'description' => 'Working remotely and serving as a trusted advisor to Mattermost customers, I offer guidance, planning and oversight during the technical deployment and implementation process.'],
    1 => ['company' => 'TrustArc', 'url' => '//trustarc.com', 'title' => 'Technical Account Manager', 'dates' => 'September 2021 - June 2022', 'description' => 'Responsible for the successful integration of TrustArc consent and privacy products. Supporting small to enterprise-level clients. Developing strong relationships with customers and leveraging relationships to achieve retention and overall revenue growth. Assisting clients with all aspects of product implementation, design, and ongoing support. Works with various internal teams and customer executives, legal, marketing, and technical teams to resolve customer challenges. Offering high-level product advice and guidance on global legal standards and best practices.'],
    2 => ['company' => 'GitLab', 'url' => '//about.gitlab.com', 'title' => 'Technical Account Manager', 'dates' => 'January 2019 – September 2021', 'description' => 'Working remotely and serving as a trusted advisor to GitLab customers, I offer guidance, planning and oversight during the technical deployment and implementation process. Filling a unique space in the overall service lifecycle and customer journey and actively binding together sales, solution architects, customer stakeholders, product management, implementation engineers and support. Started on an unmanaged team of 2 and assisted with hiring and growing the team to over 11 including a new team manager. Oversight of an account portfolio in excess of $4M and more than 40 US West Coast accounts. Providing high-level and detailed demos and guidance to new and existing customers. Responsible for customer product adoption strategy and growth through detailed organizational DevSecOps transformations. Working with customer executives and various stakeholders to showcase overall product value. Acting as the liaison and single point of contact between the customer and company in all account management aspects. Facilitating and triaging customer bug, feature, and support requests.'],
    3 => ['company' => 'YEGTEL Communications', 'url' => '//www.yegtel.ca', 'title' => 'Implementation Project Manager', 'dates' => 'July 2018 – January 2019', 'description' => 'Design, installation, and support of extensive Linux and Windows-based infrastructures, both internally and externally. Resolving high-level/visibility challenges such as disasters and outages for large clients. Responsible for the deployment and management of databases, email applications, Windows/Linux servers, cloud hosting, and VoIP phone systems. Managing client computing, network and telecommunications roll-outs and projects. Application and website design and development. Overseeing and managing staffing and project resources as well as working with vendors to source hardware/software. Assist with Sales and Marketing planning as well as assistance and technical guidance on producing bids, proposals, and quotes for public and private sector projects. Budgeting and Financial Management. Disaster recovery planning and backup management.'],
    4 => ['company' => 'Oracle Corporation', 'url' => '//www.oracle.com', 'title' => 'Field Support Engineer', 'dates' => 'March 2011 – July 2018', 'description' => 'Providing on-site diagnostics, repair and initial installation, Configuration and consulting for all StorageTek/Sun/Oracle/Micros hardware (including EXA cloud platforms). Responsible for providing consulting and implementation of highly available systems for large enterprises and multiple levels of local government. Extensive Canada/USA travel for training, maintenance, and installation activities. Highly engaged in working directly with customer\'s GNU/Linux and Solaris environments, assisting with designing and implementing solutions, as well as resolving complex technical challenges.'],
    5 => ['company' => 'ENTELIT Solutions', 'url' => '', 'title' => 'Principal Technical Architect', 'dates' => 'September 2010 – March 2011', 'description' => 'Responsible for designing and implementing production VoIP infrastructure and cloud services. Designed and deployed network infrastructure to support customers\' production web and telecom hosting environments. Working with AWS to create and deploy images, resources and networking services.'],
    6 => ['company' => 'Microserve', 'url' => '//www.microserve.ca/', 'title' => 'Geographical Support Analyst', 'dates' => 'March 2009 – August 2010', 'description' => 'Assigned to the City of Edmonton. Computer re-imaging, repairing trouble and providing guidance on Microsoft Windows and Office suite. Diagnosing problems with hardware/software, and escalating incidents to higher tiers for resolution. Troubleshooting user problems relating to operating system issues, network connectivity, TCP/IP configurations, upgrades, Microsoft Office products and internet connectivity.'],
    7 => ['company' => 'Shaw Communications', 'url' => '//www.shaw.ca', 'title' => 'Service Technician', 'dates' => 'February 2008 – March 2009', 'description' => 'Responding to customers’ homes/businesses to troubleshoot and repair television, internet, and phone reception problems. Climbing ladders and poles to investigate signal and repair or replace the coaxial cable. Educate users on the use of equipment. Install and replace the hardware required for the operation of service.'],
    8 => ['company' => 'Schlumberger Oilfield Services', 'url' => '//www.slb.com/', 'title' => 'Support Analyst', 'dates' => 'March 2007 - February 2008', 'description' => 'Managed all aspects of Information Technology for multiple offices located in Alberta. Diagnostics, Configuration, and testing of Mobile devices, Servers, Desktops, Notebooks, Printers, and Networking Equipment. Equipment installation, imaging and configuration.'],
    9 => ['company' => 'Metafore', 'url' => '', 'title' => 'Field Support Technician', 'dates' => 'January 2006 - March 2007', 'description' => 'Responding to various government and private sector businesses to repair, replace, or troubleshoot printer, desktop and server hardware/software, point of sale, networking equipment, and wireless/mobile devices.'],
    10 => ['company' => 'Deloitte and Touche', 'url' => '//www.deloitte.ca', 'title' => 'Network Analyst', 'dates' => 'December 2004 – January 2006', 'description' => 'Provided in-house support and training for more than 200 users. Managing servers, networking equipment, desktops/laptops, Nortel telecom equipment, printers, and mobile devices. Setup and training of all new hires on office technology as well as office policies and procedures. Setup and tear-down of audio/visual equipment for meetings and events.'],
    11 => ['company' => 'IBM', 'url' => '//www.ibm.com', 'title' => 'Helpdesk Analyst', 'dates' => 'July 2003 – December 2004', 'description' => 'Assigned to Fluor Global. Responding to incoming technical support phone calls, emails and requests. Troubleshooting and logging of support requests regarding hardware/software, remote access, network connections, and telephone equipment.'],
    12 => ['company' => 'Acrodex', 'url' => '', 'title' => 'Helpdesk Analyst', 'dates' => 'July 2002 – July 2003', 'description' => 'Assigned to Syncrude and Epcor. Responding to incoming technical support phone calls, emails and requests. Troubleshooting and logging of support requests regarding hardware/software, remote access, network connections, and telephone equipment.'],
    // 0 => ['company' => '', 'url' => '', 'title' => '', 'dates' => '', 'description' => ''],
];

$educationDetails = [
    0 => ['institution' => 'Athabasca University', 'url' => '//athabascau.ca', 'dates' => 'March 2022', 'description' => 'Bachelor of Commerce - Business Technology Management'],
    1 => ['institution' => 'Oracle University', 'url' => '//education.oracle.com', 'dates' => 'March 2011 - July 2018', 'description' => 'Exadata, Exalogic, Sparc, M-Series, Solaris, and various other internal certifications.'],
    2 => ['institution' => 'Microsoft Certified Professional', 'url' => '//learning.microsoft.com', 'dates' => 'September 2005', 'description' => ''],
    3 => ['institution' => 'J. Percy Page Composite High School', 'url' => '//jpercypage.epsb.ca', 'dates' => 'September 1999 - June 2002', 'description' => ''],
    // 0 => array('institution' => '', 'url' => '', 'title' => '', 'dates' => '', 'description' => ''),
];

?>
    <section id="resume" class="container page-start">
        <div class="row">
            <div class="col">
                <h1 class="page-header text-center">Resume</h1>
            </div>
        </div>
        <hr>
        <!-- Skills -->
        <section id="skills">
            <div class="row">
                <div class="col">
                    <h2>Skills</h2>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p><strong>Platforms</strong>: Linux (Oracle, CentOS/Red Hat, Ubuntu, Debian, Fedora), UNIX
                        (Solaris), BSD, macOS, Windows, VMware, Citrix Xen, Oracle VM, Android, iOS</p>
                    <p><strong>Hardware Vendors</strong>: Sun/Oracle, Dell, Hewlett-Packard, IBM, Supermicro,
                        Asus</p>
                    <p><strong>Programming</strong>: PHP, Bash, C#, JavaScript/Node, Python, HTML, CSS, WordPress</p>
                    <p><strong>Databases</strong>: MySQL, Oracle, Microsoft SQL</p>
                    <p><strong>Core Applications</strong>: Active Directory/LDAP/SSO, Apache2, IIS, NGINX, PRTG,
                        Nagios<strong>,</strong> Microsoft/Open Office, Adobe CS Suite, Tivoli, Remedy, GitLab,
                        GitHub, Bitbucket/Jira, Prometheus, Grafana, Jetbrains</p>
                    <p><strong>Cloud Platforms: </strong>Google Cloud, Amazon AWS, Oracle Cloud, Microsoft Azure,
                        Kubernetes,
                        Docker</p>
                    <p><strong>Communications</strong>: Microsoft Exchange, Postfix, Zimbra, MDaemon, SIP (Asterisk,
                        3CX, Yeastar, and Freeswitch), OpenVPN, TCP/IP (DNS, BGP/WAN, 802.11 Wireless, IPv6), XMPP,
                        Microsoft Lync, Cisco, pfSense</p>
                </div>
            </div>
        </section>
        <!-- Experience -->
        <section id="experience" class="topBorder">
            <div class="row">
                <div class="col">
                    <h2>Professional Experience</h2>
                </div>
            </div>
            <?php foreach ($jobDetails as $jobDetail) { ?>
                <div class="row resumeItem">
                    <div class="col">
                        <div class="row">
                            <div class="col-9">
                                <h3><?= (!empty($jobDetail['url'])) ? $jobDetail['title'] . ' | <a href="' . $jobDetail['url'] . '" target="_blank">' . $jobDetail['company'] . '</a>' : $jobDetail['title'] . ' | ' . $jobDetail['company']; ?></h3>
                            </div>
                            <div class="col-3">
                                <h4 class="date"><?= $jobDetail['dates']; ?></h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <p><?= $jobDetail['description']; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </section>

        <!-- Education -->
        <section id="education" class="topBorder">
            <div class="row">
                <div class="col">
                    <h2>Education</h2>
                </div>
            </div>
            <?php foreach ($educationDetails as $educationDetail) { ?>
                <div class="row resumeItem">
                    <div class="col">
                        <div class="row">
                            <div class="col-9">
                                <h3><?= (!empty($educationDetail['url'])) ? '<a href="' . $educationDetail['url'] . '" target="_blank">' . $educationDetail['institution'] . '</a>' : $educationDetail['institution']; ?></h3>
                            </div>
                            <div class="col-3">
                                <h4 class="date"><?= $educationDetail['dates']; ?></h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <p><?= (!empty($educationDetail['description'])) ? $educationDetail['description'] : null; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </section>

        <!-- Additional Details -->
        <section id="additional" class="topBorder">
            <div class="row">
                <div class="col">
                    <h2>Additional Details</h2>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <ul>
                        <li>Proven relationship builder with strong interpersonal skills.</li>
                        <li>Thrive in fast-paced, demanding technology environments.</li>
                        <li>Effective time management skills.</li>
                        <li>Experience negotiating and managing high visibility disaster and high-stress
                            situations.
                        </li>
                        <li>Highly motivated to learn new techniques and technologies.</li>
                        <li>Able to notice and understand complex technical details.</li>
                        <li>Proven documentation and proposal writer with strong language skills.</li>
                        <li>Passion for creating and working with open source software.</li>
                        <li>Extensive experience with travelling across Canada and the United States for
                            business and pleasure.
                        </li>
                    </ul>
                </div>
            </div>
        </section>
    </section>
<?php

include 'inc/footer.php';
